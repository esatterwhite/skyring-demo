# Getting Started With Skyring

## install the app

```bash
# npm install
```

## Start a 2 node ring
```bash
# Start Node 1
$ DEBUG=skyring:* PORT=3001 node index.js
```

```bash
# Start Node 2
$ DEBUG=skyring:* PORT=3002 node index.js --channel:port=3455
```

## Echo server

This is a dummy app server to handle timers. In most other cases, this would be your application

```bash
$ node echo.js
```

## Create a Timer

```bash
$ curl -i -XPOST http://localhost:3000/timer -d '{
  "timeout": 5000,
  "data": "hello world!",
  "callback": {
    "transport": "http",
    "method": "post",
    "uri": "http://0.0.0.0:5555/"
  }
}'
```
