'use strict';

const http = require('http')

http.createServer( (req, res) => {  
  res.writeHead(200)
  res.end();
  req.pipe(process.stdout)
}).listen(5555);
