'use strict'
console.log(process.pid)
const Skyring = require('skyring')

const node = new Skyring({
  node: {
    app: 'skyring'
  }
}).load().listen(process.env.PORT || 3000)

function onSignal () {
  node.close((err) => {
    if (err) process.exitCode = 1
  })
}

process.on('SIGINT', onSignal)
process.on('SIGTERM', onSignal)
